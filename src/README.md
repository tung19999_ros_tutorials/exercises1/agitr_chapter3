File pubvelsafe.cpp is Ex 1a, Session 3: Communications using topics (https://sir.upc.edu/projects/rostutorials)
The node called pubvelsafe in oder to send:	
	1. A random angular velocity command and a fixed linear velocity of 1.0 when the
	 turtle is placed in a safe zone defined by a square around the center of the window.
	2. A random angular velocity and a random linear velocity (i.e. as done in pubvel.cpp) otherwise.

File pubvelsafe_b.cpp is Ex 1a, Session 3:	
Code a new node called pubvelsafe_b (in a new file pubvelsafe_b.cpp) to make the turtle smarter not to get stuck to the walls for long.

Video link Ex1: https://www.youtube.com/watch?v=Io_V3u8CXCY
Video link Ex1b: https://www.youtube.com/watch?v=ObDALkHfwTM
