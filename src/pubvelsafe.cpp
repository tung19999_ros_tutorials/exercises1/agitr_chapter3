// This program is Ex1. Session 3.
/*
1. A random angular velocity command and a fixed linear velocity of 1.0 when the turtle
is placed in a safe zone defined by a square around the center of the window.
2. A random angular velocity and a random linear velocity (i.e. as done in pubvel.cpp) otherwise.
*/
// messages for turtlesim.
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>    // For geometry_msgs::Twist
#include <stdlib.h>                 // For rand() and RAND_MAX
#include <turtlesim/Pose.h>         // For turtlesim::Pose

double X_MAX =10, Y_MAX =10;        // Position max of X,Y of turtle in the window
double X_MIN =1, Y_MIN =1;          // Position min of X,Y of turtle in the window
double ACTUAL_X =0.0, ACTUAL_Y=0.0; // Real Position

ros::Publisher pub ;

void pubVelSafe(){
    geometry_msgs::Twist msg1;

    if(((ACTUAL_X>X_MIN) && (ACTUAL_X<X_MAX )) && ((Y_MIN<ACTUAL_Y) && (ACTUAL_Y <Y_MAX))){
        msg1.linear.x = 1.0;
        msg1.angular.z = 2*double(rand())/double(RAND_MAX) - 1;
    }
    else if (ACTUAL_X >=X_MAX || ACTUAL_Y >=Y_MAX|| ACTUAL_X <= X_MIN|| ACTUAL_Y <=Y_MIN){
        if (ACTUAL_X >=X_MAX/2){
            msg1.linear.x = 0.5*double(rand())/double(RAND_MAX);
            msg1.angular.z = -(2*double(rand())/double(RAND_MAX) - 0.25);
        }
        else{
            msg1.linear.x = 0.5*double(rand())/double(RAND_MAX);
            msg1.angular.z = (2*double(rand())/double(RAND_MAX) - 0.25);
        }

    }

    pub.publish(msg1);

    // Send a message to rosout with the details.
    ROS_INFO_STREAM("Sending random velocity command:"
      << " linear=" << msg1.linear.x
      << " angular=" << msg1.angular.z);
}

void poseMessageReceived(const turtlesim::Pose& msg) {

  ACTUAL_X = msg.x;
  ACTUAL_Y = msg.y;

  ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "position=(" <<  ACTUAL_X << "," << ACTUAL_Y << ")"
    << " direction=" << msg.theta);

  pubVelSafe();
}

int main(int argc, char **argv) {
  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "publish_velocity");
  ros::init(argc, argv, "subscribe_to_pose");
  ros::NodeHandle nh;

  // Create a publisher object.
  pub = nh.advertise<geometry_msgs::Twist>(
    "turtle1/cmd_vel", 1000);

  while(ros::ok()) {


      // Create a subscriber object.
      ros::Subscriber sub = nh.subscribe("turtle1/pose", 1000,
        &poseMessageReceived);


      ros::spin();


  }
}
