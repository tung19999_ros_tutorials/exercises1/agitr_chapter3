// This program is Ex1b. Session 3.
/*
Code a new node called pubvelsafe_b (in a new file pubvelsafe_b.cpp) to make
the turtle smarter not to get stuck to the walls for long.
*/
// messages for turtlesim.
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>    // For geometry_msgs::Twist
#include <stdlib.h>                 // For rand() and RAND_MAX
#include <turtlesim/Pose.h>         // For turtlesim::Pose

double X_MAX =11, Y_MAX =11;
double X_MIN =0.02, Y_MIN =0.02;
double ACTUAL_X =0.0, ACTUAL_Y=0.0, ACTUAL_Z=0.0;

ros::Publisher pub ;

void pubVelSafe(){
    geometry_msgs::Twist msg1;

    if(((ACTUAL_X>X_MIN) && (ACTUAL_X<X_MAX )) && ((Y_MIN<ACTUAL_Y) && (ACTUAL_Y <Y_MAX))){
        msg1.linear.x = 1.0;
        msg1.angular.z = 2*double(rand())/double(RAND_MAX) - 1;
    }
    else if (ACTUAL_X >=X_MAX || ACTUAL_Y >=Y_MAX|| ACTUAL_X <= X_MIN|| ACTUAL_Y <=Y_MIN){
        // turtle stuck
        msg1.angular.z = 2.0;
        msg1.linear.x = 0.5;
    }

    pub.publish(msg1);

    // Send a message to rosout with the details.
    ROS_INFO_STREAM("Sending random velocity command:"
      << " linear=" << msg1.linear.x
      << " angular=" << msg1.angular.z);
}

void poseMessageReceived(const turtlesim::Pose& msg) {

  ACTUAL_X = msg.x;
  ACTUAL_Y = msg.y;
  ACTUAL_Z = msg.theta;

  ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "position=(" <<  ACTUAL_X << "," << ACTUAL_Y << ")"
    << " direction=" << msg.theta);

  pubVelSafe();
}

int main(int argc, char **argv) {
  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "publish_velocity");
  ros::init(argc, argv, "subscribe_to_pose");
  ros::NodeHandle nh;

  // Create a publisher object.
  pub = nh.advertise<geometry_msgs::Twist>(
    "turtle1/cmd_vel", 1000);

  // Seed the random number generator.
  //srand(time(0));

  // Loop at 2Hz until the node is shut down.
  //ros::Rate rate(2);
  while(ros::ok()) {


      // Create a subscriber object.
      ros::Subscriber sub = nh.subscribe("turtle1/pose", 1000,
        &poseMessageReceived);

      // Wait until it's time for another iteration.
      //rate.sleep();
      ros::spin();


  }
}
